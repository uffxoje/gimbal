


/************************************************
读取AS5600的角度(模拟I2C)
PB10--SCL
PB11--SDA
=================================================
本程序仅供学习，引用代码请标明出处
使用教程：https://blog.csdn.net/loop222/article/details/120431638
          《SimpleFOC移植STM32（三）—— 角度读取》
创建日期：20210909
作    者：loop222 @郑州
************************************************/
/******************************************************************************/
#define  RAW_Angle_Hi    0x0C
#define  RAW_Angle_Lo    0x0D
#define  AS5600_Address  0x36
/***************************************************************************/
#define SDA_IN0()  {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=0x80000000;}
#define SDA_OUT0() {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=0x30000000;}
#define READ_SDA0  (GPIOB->IDR&(1<<7))
#define IIC_SCL_10  GPIO_SetBits(GPIOB,GPIO_Pin_6)
#define IIC_SCL_00  GPIO_ResetBits(GPIOB,GPIO_Pin_6)
#define IIC_SDA_10  GPIO_SetBits(GPIOB,GPIO_Pin_7)
#define IIC_SDA_00  GPIO_ResetBits(GPIOB,GPIO_Pin_7)
/***************************************************************************/
void delay_s0(u32 i)
{
	while(i--);
}
/***************************************************************************/
void IIC_Start0(void)
{
	IIC_SDA_10;
	IIC_SCL_10;
	delay_s0(20);
	IIC_SDA_00;
	delay_s0(20);
	IIC_SCL_00;
}
/***************************************************************************/
void IIC_Stop0(void)
{
	IIC_SCL_00;
	IIC_SDA_00;
	delay_s0(20);
	IIC_SCL_10;
	IIC_SDA_10;
	delay_s0(20);
}
/***************************************************************************/
//1-fail,0-success
u8 IIC_Wait_Ack0(void)
{
	u8 ucErrTime=0;
	
	SDA_IN0();
	IIC_SDA_10;
	IIC_SCL_10;
	delay_s0(10);
	while(READ_SDA0!=0)
	{
		if(++ucErrTime>250)
			{
				SDA_OUT0();
				IIC_Stop0();
				return 1;
			}
	}
	SDA_OUT0();
	IIC_SCL_00;
	return 0; 
}
/***************************************************************************/
void IIC_Ack0(void)
{
	IIC_SCL_00;
	IIC_SDA_00;
	delay_s0(20);
	IIC_SCL_10;
	delay_s0(20);
	IIC_SCL_00;
}
/***************************************************************************/
void IIC_NAck0(void)
{
	IIC_SCL_00;
	IIC_SDA_10;
	delay_s0(20);
	IIC_SCL_10;
	delay_s0(20);
	IIC_SCL_00;
}
/***************************************************************************/
void IIC_Send_Byte0(u8 txd)
{
	u32 i;
	
	IIC_SCL_00;
	for(i=0;i<8;i++)
	{
		if((txd&0x80)!=0)IIC_SDA_10;
		else
			IIC_SDA_00;
		txd<<=1;
		delay_s0(20);
		IIC_SCL_10;
		delay_s0(20);
		IIC_SCL_00;
		delay_s0(20);
	}
}
/***************************************************************************/
u8 IIC_Read_Byte0(u8 ack)
{
	u8 i,rcv=0;
	
	SDA_IN0();
	for(i=0;i<8;i++)
	{
		IIC_SCL_00; 
		delay_s0(20);
		IIC_SCL_10;
		rcv<<=1;
		if(READ_SDA0!=0)rcv++;
		delay_s0(10);
	}
	SDA_OUT0();
	if(!ack)IIC_NAck0();
	else
		IIC_Ack0();
	return rcv;
}
/***************************************************************************/
u8 AS5600_ReadOneByte0(u8 addr)
{
	u8 temp;		  	    																 
	
	IIC_Start0();
	IIC_Send_Byte0(AS5600_Address<<1);
	IIC_Wait_Ack0();
	IIC_Send_Byte0(addr);
	IIC_Wait_Ack0();	    
	IIC_Start0();  	 	   
	IIC_Send_Byte0((AS5600_Address<<1)+1);
	IIC_Wait_Ack0();	 
	temp=IIC_Read_Byte0(0);		   
	IIC_Stop0();
	
	return temp;
}
/***************************************************************************/
u16 AS5600_ReadRawAngleTwo0(void)
{
	u8 dh,dl;		  	    																 
	
	IIC_Start0();
	IIC_Send_Byte0(AS5600_Address<<1);
	IIC_Wait_Ack0();
	IIC_Send_Byte0(RAW_Angle_Hi);
	IIC_Wait_Ack0();
	IIC_Start0();
	IIC_Send_Byte0((AS5600_Address<<1)+1);
	IIC_Wait_Ack0();
	dh=IIC_Read_Byte0(1);   //1-ack for next byte
	dl=IIC_Read_Byte0(0);   //0-end trans
	IIC_Stop0();
	
	return ((dh<<8)+dl);
}
/***************************************************************************/


